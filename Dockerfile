FROM python:3.10.13-slim-bookworm

WORKDIR /app

RUN apt-get -y update && apt-get -y upgrade && apt-get install libgomp1
COPY requirements.txt /app/requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

COPY src/ src/

EXPOSE 8080
CMD ["uvicorn", "src.main:app", "--host", "0.0.0.0", "--port", "8080"]