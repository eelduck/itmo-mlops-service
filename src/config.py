"""Config"""

import os

from dotenv import load_dotenv
from pydantic_settings import BaseSettings

load_dotenv()


class Settings(BaseSettings):
    PROJECT_ROOT: str = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    # mlflow
    MLFLOW_PORT: int = 5000
    MLFLOW_HOST: str = "87.242.85.31"

    mlflow_uri: str = f"http://{MLFLOW_HOST}:{MLFLOW_PORT}"
    model_name: str = "lightgbm_model"

    # data
    TARGET: str = "is_promoted"
    RANDOM_STATE: int = 42

    HIGH_AVG_DEPARTMENTS: list[str] = [
        "Technology",
        "Analytics",
        "R&D",
    ]  # high average training score departments
    MID_AVG_DEPARTMENTS: list[str] = [
        "Operations",
        "Finance",
        "Procurement",
        "Legal",
    ]  # medium average training score departments
    LOW_AVG_DEPARTMENTS: list[str] = [
        "Sales & Marketing",
        "HR",
    ]  # low average training score departments

    COLS_TO_DROP: list[str] = ["employee_id", "gender", "education"]
    OHE_FEATURES: list[str] = ["department", "region", "recruitment_channel"]

    REQUIRED_COLUMNS: list[str] = [
        "employee_id",
        "department",
        "region",
        "education",
        "gender",
        "recruitment_channel",
        "no_of_trainings",
        "age",
        "previous_year_rating",
        "length_of_service",
        "KPIs_met >80%",
        "awards_won?",
        "avg_training_score",
    ]


settings = Settings()
