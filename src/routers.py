from functools import lru_cache

import pandas as pd
from fastapi import APIRouter, Depends, File, HTTPException, UploadFile
from mlflow.tracking import MlflowClient

from src.config import settings
from src.model import LightGBMModel
from src.schemas import PredictionsResponse

predict_router = APIRouter()


# Dependency Injection
@lru_cache
def get_model() -> LightGBMModel:
    return LightGBMModel(settings.model_name, mlflow_client=MlflowClient())


@predict_router.post("/predict")
async def predict_csv(file: UploadFile = File(...), model: LightGBMModel = Depends(get_model)) -> PredictionsResponse:
    try:
        input_df = pd.read_csv(file.file)

        if not all(column in input_df.columns for column in settings.REQUIRED_COLUMNS):
            raise HTTPException(status_code=400, detail="CSV file is missing required columns")

        predictions = model.predict(input_df)

        return PredictionsResponse(predictions=predictions)

    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
