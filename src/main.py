from collections.abc import AsyncGenerator
from contextlib import asynccontextmanager

import mlflow
from fastapi import FastAPI
from prometheus_fastapi_instrumentator import Instrumentator

from src.config import settings
from src.routers import predict_router


@asynccontextmanager
async def lifespan(app_instance: FastAPI) -> AsyncGenerator:
    """Function that handles startup and shutdown events.
    To understand more, read https://fastapi.tiangolo.com/advanced/events/
    """
    mlflow.set_tracking_uri(settings.mlflow_uri)
    yield


def create_app() -> FastAPI:
    """Creates app instance"""
    application = FastAPI(
        lifespan=lifespan,
        openapi_url="/openapi.json",
        docs_url="/swagger",
    )

    routers = [predict_router]
    for router in routers:
        application.include_router(router)

    return application


app = create_app()
Instrumentator().instrument(app).expose(app)
