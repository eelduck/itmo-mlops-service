import os

import joblib
import mlflow
import pandas as pd
from mlflow.tracking import MlflowClient

from src.config import settings
from src.processing import base_processing


class LightGBMModel:
    def __init__(self, model_name: str, mlflow_client: MlflowClient) -> None:
        self.model_name = model_name
        self.model = None
        self.education_encoder = None
        self.ohe_encoder = None
        self.client = mlflow_client
        self._load_latest_model_and_encoders()

    def _load_latest_model_and_encoders(self) -> None:
        latest_versions = self.client.get_latest_versions(self.model_name, stages=["None"])
        latest_version = latest_versions[0].version
        latest_run_id = latest_versions[0].run_id

        model_uri = f"models:/{self.model_name}/{latest_version}"
        self.model = mlflow.lightgbm.load_model(model_uri)

        # Download and load encoders
        with mlflow.start_run(run_id=latest_run_id):
            local_path = mlflow.artifacts.download_artifacts(run_id=latest_run_id, artifact_path="encoders")

            self.education_encoder = joblib.load(os.path.join(local_path, "education_encoder.pkl"))
            self.ohe_encoder = joblib.load(os.path.join(local_path, "ohe_encoder.pkl"))

    def _preprocess(self, input_df: pd.DataFrame) -> pd.DataFrame:
        """Apply encoders to the input data"""
        input_df = base_processing(input_df)
        input_df["education"] = self.education_encoder.transform(input_df)
        encoded_df = self.ohe_encoder.transform(input_df)
        input_df = input_df.drop(columns=settings.OHE_FEATURES)
        input_df = pd.concat([input_df, encoded_df], axis=1)
        return input_df

    def predict(self, input_df: pd.DataFrame) -> pd.Series:
        """Make predictions on the preprocessed input data"""
        preprocessed_df = self._preprocess(input_df)
        predictions = self.model.predict(preprocessed_df)
        return predictions.tolist()
