import numpy as np
import pandas as pd

from src.config import settings


def fill_missing_values(input_df: pd.DataFrame) -> pd.DataFrame:
    """Fills missing values in the provided data CSV file and saves the results."""

    fill_values = {"education": "missing", "previous_year_rating": 3}

    return input_df.fillna(value=fill_values)


def gender_binarization(input_df: pd.DataFrame) -> pd.DataFrame:
    """Encodes gender column with binary values."""
    input_df["gender"] = input_df["gender"].apply(lambda x: 1 if x == "m" else 0)

    return input_df


def calc_department_avg(input_df: pd.DataFrame) -> pd.DataFrame:
    """Maps departments to average training scores"""
    department_mapping = {
        **{dept: 2 for dept in settings.HIGH_AVG_DEPARTMENTS},
        **{dept: 1 for dept in settings.MID_AVG_DEPARTMENTS},
        **{dept: 0 for dept in settings.LOW_AVG_DEPARTMENTS},
    }
    input_df["department_avg"] = input_df["department"].replace(department_mapping)

    return input_df


def log_transform(input_df: pd.DataFrame, columns: list[str]) -> pd.DataFrame:
    input_df = input_df.apply(lambda x: np.log(x) if x.name in columns else x, axis=0)

    return input_df


def base_processing(input_df: pd.DataFrame) -> pd.DataFrame:
    """Processes the input data CSV file and saves the transformed data."""
    input_df = fill_missing_values(input_df)
    input_df = gender_binarization(input_df)
    input_df = calc_department_avg(input_df)
    input_df = log_transform(input_df, ["age"])

    return input_df
